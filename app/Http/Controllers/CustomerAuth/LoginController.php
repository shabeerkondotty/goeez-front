<?php

namespace App\Http\Controllers\CustomerAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Class needed for login and Logout logic
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{

    //Where to redirect seller after login.
    protected $redirectTo = '/';

    use AuthenticatesUsers;
    //TODO : issue with logout - if a customer session expired due to idle or click logout after expiration.
    protected function credentials(Request $request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
            ? $this->username()
            : 'phone';

        return [
            $field => $request->get($this->username()),
            'password' => $request->password,
        ];
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect($this->redirectTo);
    }

    protected function guard()
    {
        return Auth::guard('web_customer');
    }

    //Shows customer login form
    public function showLoginForm()
    {
        return view('auth.login');
    }
}
