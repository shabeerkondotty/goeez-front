<?php

namespace App\Http\Controllers\CustomerAuth;

use App\Category;
use App\Http\Utilities\CommonUtility;
use App\Mail\SendOtp;
use App\Website\CustomerDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Website\Customer;
use Auth;
use Illuminate\Validation\Rule;

class RegisterController extends Controller
{

    protected $redirectPath = '/';

    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    {
        //Validates data
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            if($request->ajax()) {
                return response()->json($validator->errors(), 422);
            } else {
                return redirect()->route('customer.signup')
                    ->withErrors($validator)
                    ->withInput();
            }
        }else {
            $input = $request->only(['name','email','password', 'phone','nationality']);
            //Create customer
            $customer = $this->create($input);
            /*$this->account_conf_email($customer);*/
            if ($request->ajax()) {
                return response()->json();
            } else {
                return redirect()->route('customer.signup');
            }
        }
    }

    /*public  function showOtp() {
        return view('auth.otp_confirm');
    }*/

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'email' => 'required|email|max:255|unique:customers',
            /*'phone' => 'required|min:10|max:10|unique:customers',*/
            'phone' => 'required|unique:customers',
            'password' => 'bail|required|min:6|confirmed',
            'nationality' => 'required',
        ]);
        //TODO : validation - email or phone anyone should make mandatory
    }

    protected function create(array $data)
    {
        $customer = new Customer;

        $customer->fill([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => bcrypt($data['password']),
            'nationality' => $data['nationality'],
            'status' => 3,
            'is_active' => 1,
        ]);
        $customer->save();

        $customer_detail = new CustomerDetail;
        $customer_detail->fill([
            'customer_id' => $customer->id,
            'address' => [],
            'profile_pic' => '',
        ]);
        $customer_detail->save();

        //Authenticates customer
        $this->guard()->login($customer);

        return $customer;
    }

    protected function account_conf_email($customer) {
        $otp = CommonUtility::otp();
        session(['goeezz_reg_o_t_p' => $otp]);
        session(['goeezz_reg_cust_temp' => $customer->id]);
        if(!empty($customer->email)) {
            Mail::to($customer->email)->send(new SendOtp($customer->name));
        }
    }

    public  function resend_account_conf_email() {

    }

    public  function validate_otp(Request $request) {

        $messages = [
            'goeez_otp.required' => 'OTP is required.',
        ];
        $validator = Validator::make($request->all(), [
            'goeez_otp' => 'required',
        ],$messages);
        if ($validator->fails()) {
            /*return response()->json($validator->errors(), 422);*/
            return redirect()->route('customer.otp.view')
                ->withErrors($validator)
                ->withInput();

        }
        else {
            $otp = $request->session()->get('goeezz_reg_o_t_p');
            $get_otp = (int)$request->goeez_otp;
            if($otp == $get_otp) {
                $customer_id = $request->session()->get('goeezz_reg_cust_temp');
                $customer = Customer::find($customer_id);
                //Authenticates customer
                $this->guard()->login($customer);
                //Redirects customer
                return redirect($this->redirectPath);
            }else {
                $validator->getMessageBag()->add('goeez_otp', 'Incorrect OTP Entered..');
                return redirect()->route('customer.otp.view')
                    ->withErrors($validator)
                    ->withInput();
            }
        }


    }

    protected function guard()
    {
        return Auth::guard('web_customer');
    }

    public function test() {
        return view('auth.test');
    }
}
