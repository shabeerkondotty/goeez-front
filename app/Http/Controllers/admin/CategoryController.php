<?php

namespace App\Http\Controllers\admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use yajra\DataTables\Facades\DataTables;
use Validator;
use File;
use Storage;

class CategoryController extends Controller
{
    public  function index() {
        return view('pages.categories.index');
    }
    public  function data() {

        return Datatables::eloquent(Category::select())
            ->rawColumns(['name','parent','action'])
            ->editColumn('name', function ($modal) {
                return '<p>' . $modal->name . '</p>';
            })
            ->addColumn('parent', function ($modal) {
                return $modal->parent_name();
            })
            ->addColumn('action', function ($modal) {
                $publishIcon = $modal->is_active==1 ? 'fa-power-off' : 'fa-circle-o-notch';
                $publishTitle = $modal->is_active==1 ? 'Unpublish' : 'Publish';
                return '<a data-plugin="render-modal" data-modal="#dvAdd-category" data-target="' . route('admin.categories.edit_modal',[$modal->id]) . '" title="Edit" > <i class="fa fa-pencil text-primary"></i></a>
                                &nbsp;&nbsp;&nbsp;&nbsp;<a data-action="' . route('admin.categories.delete',[$modal->id]) . '" href="" data-plugin="ajaxGetRequest" data-conf-message="Are you sure to delete..?" data-type="DELETE" title="Delete"> <i class="fa fa-trash text-primary"></i></a>
                                &nbsp;&nbsp;&nbsp;&nbsp;<a data-action="' . route('admin.categories.change_status',[$modal->id]) . '" href="" data-plugin="ajaxGetRequest" data-type="GET" data-formdata="' . $modal->is_active . '" title="'. $publishTitle . '"> <i class="fa '. $publishIcon . ' text-primary"></i></a>';
            })
            ->setRowId('category_{{$id}}')
            ->make(true);
    }

    public function create_modal()
    {
        $categoryLists = Category::where('parent','0')->get();
        $returnHTML = view('pages.categories.add-modal-form',['categoryLists' => $categoryLists])->render();
        return response()->json(['html' => $returnHTML]);
    }

    public  function store(Request $request) {
        $rules = [
            'name' => 'required'
        ];
        $messages = [
            'required' => 'The :attribute field is required.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if($request->ajax()) {
                return response()->json($validator->errors(), 422);
            } else {
                return redirect()->route('admin.lab.customers.create')
                    ->withErrors($validator)
                    ->withInput($request->all());
            }
        }
        $category = new Category;
        $input['name'] = $request->name;
        $input['parent'] = empty($request->parent) ? 0 : $request->parent;
        $category->fill($input)->save();

        if($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = $category->id . '_' . str_replace(' ','_',$image->getClientOriginalName());
            $destinationPath = storage_path("app" . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . Category::FILE_DIRECTORY);
            if (!File::exists($destinationPath)) {
                File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }
            $realPath = $_FILES['image']['tmp_name'];
            $contents = file_get_contents($realPath);
            $image_path = Category::FILE_DIRECTORY . '/'. $image_name;
            Storage::disk('categories')->put($image_name, $contents);
            $category->image = $image_path;
        }

        if ($request->ajax()) {
            return response()->json(['customer'=>$category, 'success' => 'New Category added successfully']);
        } else {
            return redirect()->route('admin.categories.index')->with(['success' => 'New Category added successfully']);
        }
    }

    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return response()->json(['category' => $category]);
    }

    public function edit_modal($id)
    {
        $category = Category::findOrFail($id);
        $categoryLists = Category::where('parent','0')->where('id','!=',$id)->get();
        $returnHTML = view('pages.categories.add-modal-form',['categoryLists' => $categoryLists, 'category' => $category])->render();
        return response()->json(['html' => $returnHTML]);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required'
        ];
        $messages = [
            'required' => 'The :attribute field is required.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if($request->ajax()) {
                return response()->json($validator->errors(), 422);
            } else {
                return redirect()->route('admin.categories.edit', ['id' => $id])
                    ->withErrors($validator)
                    ->withInput($request->all());
            }
        }
        $category = Category::find($id);
        $input['name'] = $request->name;
        $input['parent'] = empty($request->parent) ? 0 : $request->parent;

        if($request->hasFile('image')) {
            if(!empty($category->image) && $category->image != null){
                Storage::delete('public/' . $category->image);
            }
            $image = $request->file('image');
            $image_name = $category->id . '_' . str_replace(' ','_',$image->getClientOriginalName());
            $destinationPath = storage_path("app" . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . Category::FILE_DIRECTORY);
            if (!File::exists($destinationPath)) {
                File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }
            $realPath = $_FILES['image']['tmp_name'];
            $contents = file_get_contents($realPath);
            $image_path = Category::FILE_DIRECTORY . '/'. $image_name;
            Storage::disk('prod_cats')->put($image_name, $contents);
            $input['image'] = $image_path;
        }
        else {
            if(isset($request->is_image) && ($request->is_image == 0)){
                Storage::delete('public/' . $category->image);
                $input['image'] = null;
            }
        }
        $category->fill($input)->save();

        if ($request->ajax()) {
            return response()->json(['success' => 'Category has been updated successfully']);
        } else {
            return redirect()->route('admin.categories.index')->with(['success' => 'category has been updated successfully']);
        }
    }

    public function destroy($id)
    {
        Category::find($id)->delete();
        return response()->json(['success' => 'Category has been deleted successfully']);
    }

    public function change_status(Request $request, $id)
    {

        $changeStatus = $request->value == 1 ? 0 : 1;
        $new_status = $request->value == 1 ? 'inactive' : 'active';
        $model = Category::find($id);
        if($model) {
            $model->is_active = $changeStatus;
            $model->save();
        }

        return response()->json(['success' => 'Status has been changed to ' . $new_status . ' successfully']);
    }
}
