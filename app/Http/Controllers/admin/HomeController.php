<?php

namespace App\Http\Controllers\admin;
use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages.home');
    }

    public function test()
    {
        $product = Product::with('categories')->find(2);
        $product_category_array = $product->categories;
        foreach($product_category_array as $product_category) {
            $product_categories[] = $product_category->id;
        }
        return $product_categories;
    }
}
