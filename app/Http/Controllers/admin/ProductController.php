<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\PriceDetail;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Uuid;
use yajra\DataTables\Facades\DataTables;
use Validator;
use File;
use Storage;

class ProductController extends Controller
{
    public  function index() {

        return view('pages.products.index');
    }
    public  function data() {
        $products = Product::select(['id','name','prod_stock','is_active','created_at'])->latest();
        return DataTables::of($products)
            /*return Datatables::eloquent(Product::select())*/
            ->rawColumns(['name','prod_stock','action'])
            ->editColumn('name', function ($modal) {
                return '<p>' . $modal->name . '</p>';
            })
            ->editColumn('prod_stock', function ($modal) {
                return '<a href="#">' . $modal->prod_stock . '</a>';
            })
            ->addColumn('action', function ($modal) {
                $publishIcon = $modal->is_active==1 ? 'fa-power-off' : 'fa-circle-o-notch';
                $publishTitle = $modal->is_active==1 ? 'Unpublish' : 'Publish';
                return '<a  href="'. route('admin.products.edit',[$modal->id]) . '" title="Edit" > <i class="fa fa-pencil text-primary"></i></a>
                                &nbsp;&nbsp;&nbsp;&nbsp;<a data-action="' . route('admin.products.delete',[$modal->id]) . '" href="" data-plugin="ajaxGetRequest" data-conf-message="Are you sure to delete..?" data-type="DELETE" title="Delete"> <i class="fa fa-trash text-primary"></i></a>
                                &nbsp;&nbsp;&nbsp;&nbsp;<a data-action="' . route('admin.products.change_status',[$modal->id]) . '" href="" data-plugin="ajaxGetRequest" data-type="GET" data-formdata="' . $modal->is_active . '" title="'. $publishTitle . '"> <i class="fa '. $publishIcon . ' text-primary"></i></a>';
            })
            ->setRowId('product_{{$id}}')
            ->make(true);
    }
    public function create() {
        return view('pages.products.add');
    }
    public function store(Request $request) {
        /*return $request->all();*/
        $rules = [
            'name' => 'required',
            'unit_om' => 'required',
        ];
        $messages = [
            'required' => 'The :attribute field is required.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if($request->ajax()) {
                return response()->json($validator->errors(), 422);
            } else {
                return redirect()->route('admin.products.create')
                    ->withErrors($validator)
                    ->withInput($request->except('password','password_confirm'));
            }
        }
        else {
            $product = new Product;
            $input = $request->only(['name','description','prod_stock', 'unit_om']);
            $input['uuid'] = Uuid::generate();
            $input['is_active'] = 1;
            $input['is_featured'] = isset($request->is_featured) ? 1 : 0;

            $product->fill($input)->save();

            if($request->hasFile('image')) {
                $image = $request->file('image');
                $image_name = $product->id . '_' . str_replace(' ','_',$image->getClientOriginalName());
                $destinationPath = storage_path("app" . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . Product::FILE_DIRECTORY);
                if (!File::exists($destinationPath)) {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $realPath = $_FILES['image']['tmp_name'];
                $contents = file_get_contents($realPath);
                $image_path = Product::FILE_DIRECTORY . '/'. $image_name;
                Storage::disk('products')->put($image_name, $contents);
                $product->image = $image_path;
            }

            if($request->hasFile('brochure')) {
                $brochure = $request->file('brochure');
                $brochure_name = $product->id . '_' . str_replace(' ','_',$brochure->getClientOriginalName());
                $destinationPath_bro = storage_path("app" . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . Product::FILE_DIRECTORY_BROCHURE);
                if (!File::exists($destinationPath_bro)) {
                    File::makeDirectory($destinationPath_bro, $mode = 0777, true, true);
                }
                $realPath = $_FILES['brochure']['tmp_name'];
                $contents = file_get_contents($realPath);
                $brochure_path = Product::FILE_DIRECTORY_BROCHURE . '/'. $brochure_name;
                Storage::disk('brochures')->put($brochure_name, $contents);
                $product->brochure = $brochure_path;
            }

            $product->save();
            $product->uuid = $product->id.$product->uuid;
            $product->save();
            $product_single = $product->find($product->id);

            foreach($request->type_size as $index=>$type_size) {
                foreach($request->quantity_from[$index] as $index2=>$quantity_from) {
                    $quantity_to = $request->quantity_to[$index][$index2];
                    $price = $request->price[$index][$index2];
                    $product_single->prices()->saveMany([
                        new PriceDetail(['product_id' => $product->id, 'type_size' =>$type_size, 'quantity_from' =>$quantity_from, 'quantity_to' =>$quantity_to, 'price' =>$price]),
                    ]);
                }
            }

            if($request->has('category')) {
                $categories = Product::find($product->id);
                $categories->categories()->attach($request->category);
            }

            if ($request->ajax()) {
                return response()->json(['success' => 'New product has been added successfully']);
            } else {
                return redirect()->route('admin.products.index')->with('success', 'New product has been added successfully');
            }
        }
    }
    public function edit($id) {
        $product = Product::findOrFail($id);
        $type_sizes = PriceDetail::where('product_id',$id)->distinct('type_size')->get(['type_size']);
        foreach($type_sizes as $key=>$type_size) {
            $quantities = PriceDetail::where('product_id',$id)->where('type_size','=',$type_size->type_size)->get(['quantity_from','quantity_to','price']);
            $type_size->quantities = $quantities;
        }

        return view('pages.products.add',['product'=>$product,'type_sizes'=>$type_sizes]);
    }
    public function update(Request $request, $id) {
        $rules = [
            'name' => 'required',
            'unit_om' => 'required',
        ];
        $messages = [
            'required' => 'The :attribute field is required.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if($request->ajax()) {
                return response()->json($validator->errors(), 422);
            } else {
                return redirect()->route('admin.products.create')
                    ->withErrors($validator)
                    ->withInput($request->except('password','password_confirm'));
            }
        }
        else {
            $product = Product::find($id);
            $input = $request->only(['name','description','prod_stock', 'unit_om']);
            $input['is_featured'] = isset($request->is_featured) ? 1 : 0;

            if($request->hasFile('image')) {
                if(!empty($product->image) && $product->image != null){
                    Storage::delete('public/' . $product->image);
                }
                $image = $request->file('image');
                $image_name = $product->id . '_' . str_replace(' ','_',$image->getClientOriginalName());
                $destinationPath = storage_path("app" . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . Product::FILE_DIRECTORY);
                if (!File::exists($destinationPath)) {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $realPath = $_FILES['image']['tmp_name'];
                $contents = file_get_contents($realPath);
                $image_path = Product::FILE_DIRECTORY . '/'. $image_name;
                Storage::disk('products')->put($image_name, $contents);
                $input['image'] = $image_path;
            }
            else {
                if(isset($request->is_image) && ($request->is_image == 0)){
                    Storage::delete('public/' . $product->image);
                    $input['image'] = null;
                }
            }
            if($request->hasFile('brochure')) {
                if(!empty($product->brochure) && $product->brochure != null){
                    Storage::delete('public/' . $product->brochure);
                }
                $brochure = $request->file('brochure');
                $brochure_name = $product->id . '_' . str_replace(' ','_',$brochure->getClientOriginalName());
                $destinationPath_bro = storage_path("app" . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . Product::FILE_DIRECTORY_BROCHURE);
                if (!File::exists($destinationPath_bro)) {
                    File::makeDirectory($destinationPath_bro, $mode = 0777, true, true);
                }
                $realPath = $_FILES['brochure']['tmp_name'];
                $contents = file_get_contents($realPath);
                $brochure_path = Product::FILE_DIRECTORY_BROCHURE . '/'. $brochure_name;
                Storage::disk('brochures')->put($brochure_name, $contents);
                $input['brochure'] = $brochure_path;
            }
            else {
                if(isset($request->is_brochure) && ($request->is_brochure == 0)){
                    Storage::delete('public/' . $product->brochure);
                    $input['brochure'] = null;
                }
            }
            $product->fill($input)->save();

            $product->prices()->delete();
            foreach($request->type_size as $index=>$type_size) {
                foreach($request->quantity_from[$index] as $index2=>$quantity_from) {
                    $quantity_to = $request->quantity_to[$index][$index2];
                    $price = $request->price[$index][$index2];
                    $product->prices()->saveMany([
                        new PriceDetail(['product_id' => $product->id, 'type_size' =>$type_size, 'quantity_from' =>$quantity_from, 'quantity_to' =>$quantity_to, 'price' =>$price]),
                    ]);
                }
            }

            if($request->has('category')) {
                $categories = Product::find($product->id);
                $categories->categories()->sync($request->category);
            }

            if ($request->ajax()) {
                return response()->json(['success' => 'New product has been added successfully']);
            } else {
                return redirect()->route('admin.products.index')->with('success', 'New product has been added successfully');
            }
        }
    }

    public function destroy($id)
    {
        Product::find($id)->delete();
        return response()->json(['success' => 'Product has been deleted successfully']);
    }

    public function change_status(Request $request, $id)
    {
        $changeStatus = $request->value == 1 ? 0 : 1;
        $new_status = $request->value == 1 ? 'inactive' : 'active';
        $model = Product::find($id);
        if($model) {
            $model->is_active = $changeStatus;
            $model->save();
        }
        return response()->json(['success' => 'Status has been changed to ' . $new_status . ' successfully']);
    }

    public function sidebar_categories($product_id='')
    {
        $categories = Category::where('parent','0')->get();
        $content = '<ul class="list-group">';
        foreach ($categories as $category) {
            $content .= $this->show_category_lists($category,$product_id);
        }
        $content .= '</ul>';

        return ['content'=>$content];
    }
    public  function show_category_lists($category,$product_id) {
        $product_categories = [];
        if($product_id !='') {
            $product = Product::find($product_id);
            $product_category_array = $product->categories;
            foreach ($product_category_array as $product_category) {
                $product_categories[] = $product_category->id;
            }
        }else {
            $product_categories = [];
        }

        $child_categories = Category::where('parent',$category->id)->get();

        $child_content = '';
        if($child_categories) {
            $child_content .= '<div style="padding-left:20px;margin-top:10px;">
                                    <ul class="list-group">';
            foreach ($child_categories as $child_category) {
                $isChecked = in_array($child_category->id, $product_categories) ? 'checked' : '';
                $child_content .= '<li class="list-group-item">
                                            <input type="checkbox" name="category[]" id="child_category_' . $child_category->id . '" value="' . $child_category->id . '" ' . $isChecked . '   />
                                            <label for="child_category_' . $child_category->id . '">
                                                ' . $child_category->name . '
                                            </label>
                                        </li>';
            }
            $child_content .= '</ul>
                                </div>';
        }

        $content = '<li class="list-group-item">
                            <div class="checkbox">
                                <input type="checkbox" name="category[]" id="category_' . $category->id . '" value="' . $category->id . '" />
                                <label for="category_' . $category->id . '">
                                    <strong>' . $category->name . '</strong>
                                </label>' . $child_content .
                            '</div>
                        </li>';
        return $content;
    }
}
