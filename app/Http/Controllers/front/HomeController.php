<?php

namespace App\Http\Controllers\front;

use App\Website\Customer;
use App\Website\CustomerDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use Illuminate\Validation\Rule;

class HomeController extends Controller
{
    public  function index () {
        return view('pages.home');
    }

    public  function details () {
        return view('auth.details');
    }

    public  function signup_complete () {

        $status = Auth::guard('web_customer')->user()->status;
        if(isset($status) && $status!=1) {
            return view('auth.complete_register');
        }else {
            return redirect()->route('index');
        }

    }

    public function register_second(Request $request)
    {
        //Validates data
        $validator = $this->validator_second($request->all());

        if ($validator->fails()) {
            if($request->ajax()) {
                return response()->json($validator->errors(), 422);
            } else {
                return redirect()->route('customer.signup')
                    ->withErrors($validator)
                    ->withInput();
            }
        }else {

            $input = $request->only(['is_new','family_type','living_type','total_members','total_bed','total_room']);

            $budget = explode('-',$request->budget);
            $budget_f = $budget[0];
            $budget_t = $budget[1];
            $input['budget_f'] = $budget_f;
            $input['budget_t'] = $budget_t;
            $input['staying'] = $request->is_new==1 ? 0 : $request->staying;
            //Create customer
            $customer = $this->create_second($input);
            if ($request->ajax()) {
                return response()->json();
            } else {
                return redirect()->route('customer.signup');
            }
        }
    }

    protected function validator_second(array $data)
    {
        return Validator::make($data, [
            'is_new' => 'required',
            'family_type' => 'required',
            'living_type' => 'required',
            'total_members' => 'required',
            'total_bed' => 'required',
            'total_room' => 'required',
            'budget' => 'required',
        ]);
    }

    protected function create_second(array $data)
    {
        $customer_id = Auth::guard('web_customer')->user()->id;
        $customer = Customer::find($customer_id);

        $customer->fill([
            'status' => 2,
        ]);
        $customer->save();

        $customer_detail = $customer->customer_detail->fill([
            'is_new' => $data['is_new'],
            'staying' => $data['staying'],
            'family_type' => $data['family_type'],
            'living_type' => $data['living_type'],
            'total_members' => $data['total_members'],
            'total_bed' => $data['total_bed'],
            'total_room' => $data['total_room'],
            'budget_f' => $data['budget_f'],
            'budget_t' => $data['budget_t'],
        ]);
        $customer_detail->save();
        return $customer;
    }

    public function register_third(Request $request)
    {
        $rules = [
            'is_agree' => 'required',
        ];
        $messages = [
            'required' => 'Agree terms & conditions',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            if($request->ajax()) {
                return response()->json($validator->errors(), 422);
            } else {
                return redirect()->route('customer.signup')
                    ->withErrors($validator);
            }
        }
        else {
            $input = $request->only(['is_agree']);
            //Create customer

            $customer_id = Auth::guard('web_customer')->user()->id;
            $customer = Customer::find($customer_id);

            $customer->fill([
                'status' => 1,
            ]);
            $customer->save();

            $customer_detail = $customer->customer_detail->fill([
                'is_agree' => $request->is_agree
            ]);
            $customer_detail->save();

            if ($request->ajax()) {
                return response()->json();
            } else {
                return redirect()->route('customer.signup');
            }
        }
    }

}
