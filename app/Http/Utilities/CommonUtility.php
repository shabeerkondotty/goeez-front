<?php

namespace App\Http\Utilities;
use Auth;

class CommonUtility{

    //Constants

    const GOEEZ_APP_LINK = 'http://localhost/goeez-web/';

    const THEME_ADMIN = 'theme-admin/';

    const CATEGORY_ID_OFFER = 11;

    public static function get_cust_sess() {
        return session('cust_phone_auth');
    }

    public static function otp()
    {
        $otp = rand(100000, 999999);
        return $otp;
    }

    /*public  static function getModalId(){
        return [
            ''                      => self::TASKABLE_TYPE_MISC,
            "App\\HcsApplication"   => self::TASKABLE_TYPE_HCS_APPLICATION ,
            "App\\CSApplication"    => self::TASKABLE_TYPE_CS_APPLICATION,
            "App\\Property"         => self::TASKABLE_TYPE_PROPERTY,
            "App\\Unit"             => self::TASKABLE_TYPE_UNIT,
            "App\\HcsRequest"       => self::TASKABLE_TYPE_HCS_REQUEST,
            "App\\CSRequest"        => self::TASKABLE_TYPE_CS_REQUEST,
        ];
    }*/



//Application Code Format
    /*public static function hcsApplicationCode($id)
    {
        $code = 'HCS-'.str_pad($id, 4, '0', STR_PAD_LEFT);
        return $code;
    }*/


    /*protected  static $calendar_months = [
        1 => "January",
        2 => "February",
        3 => "March",
        4 => "April",
        5 => "May",
        6 => "June",
        7 => "July",
        8 => "August",
        9 => "September",
        10 => "October",
        11 => "November",
        12 => "December"
    ];
    public static function calendarMonths()
    {
        return static::$calendar_months;
    }*/
}