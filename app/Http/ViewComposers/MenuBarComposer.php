<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;

class MenuBarComposer
{


    public function __construct()
    {
        // Dependencies automatically resolved by service container...
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $menus = [
            [
                "label" => "Dashboard",
                "route" => "admin.index",
                "icon" => "fa fa-dashboard",
                "roles" => "admin",
                "sub_routes" => ['admin.index'],
                "sub_route_prefixes" => null,
            ],
            [
                "label" => "Products",
                "route" => "admin.products.index",
                "icon" => "fa fa-product-hunt",
                "sub_routes" => ['admin.products.index','admin.products.create','admin.products.edit'],
                "sub_route_prefixes" => null,
                "sub_menus" => [
                    [
                        "label" => "Products",
                        "route" => "admin.products.index",
                        "icon" => "fa fa-list",
                        "sub_routes" => ['admin.products.index','admin.products.edit'],
                        "sub_route_prefixes" => null,
                    ],
                    [
                        "label" => "Add New",
                        "route" => "admin.products.create",
                        "icon" => "fa fa-plus",
                        "sub_routes" => ['admin.products.create'],
                        "sub_route_prefixes" => null,
                    ]

                ]
            ],
            [
                "label" => "Categories",
                "route" => "admin.categories.index",
                "icon" => "fa fa-briefcase",
                "roles" => "admin",
                "sub_routes" => ['admin.categories.index'],
                "sub_route_prefixes" => null,
            ],

        ];
        $view->with('menus', $menus);
    }
}
//"/calendar"