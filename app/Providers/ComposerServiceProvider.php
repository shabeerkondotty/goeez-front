<?php

namespace App\Providers;

use App\Website\Property;
use App\Website\Unit;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['partial.compound_list'], function ($view) {
            $compounds = Unit::where('property_type',Property::PROPERTY_TYPE_COMPOUND)->latest()->get();
            $view->with('compounds',$compounds);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
