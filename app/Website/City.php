<?php

namespace App\Website;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    //
    use SoftDeletes;

    protected $fillable = ['country_id','name','city_code'];

    protected $dates = ['deleted_at'];

    public function district()
    {
        return $this->hasMany('App\Website\District','city_id', 'id');
    }
}
