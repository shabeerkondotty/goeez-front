<?php

namespace App\Website;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    protected $fillable = [
        'name', 'email', 'phone', 'password', 'nationality', 'status', 'is_active'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function customer_detail() {
        return $this->hasOne('App\Website\CustomerDetail','customer_id', 'id');
    }
}
