<?php

namespace App\Website;

use Illuminate\Database\Eloquent\Model;

class CustomerDetail extends Model
{
    protected $fillable = ['customer_id','is_new','staying','family_type','living_type','total_members','total_bed','total_room','budget_f','budget_t','is_agree','profile_pic'];

    public function customer()
    {
        return $this->belongsTO('App\Website\Customer', 'customer_id', 'id');
    }
}
