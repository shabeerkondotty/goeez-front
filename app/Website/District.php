<?php

namespace App\Website;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class District extends Model
{
    use SoftDeletes;

    protected $fillable = ['city_id','name','code','center_location','zone1_location','zone2_location','zone3_location','zone4_location'];

    protected $dates = ['deleted_at'];

    public function city()
    {
        return $this->belongsTO('App\Website\City', 'city_id', 'id');
    }
}
