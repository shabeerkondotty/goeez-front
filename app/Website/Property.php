<?php

namespace App\Website;

use Illuminate\Database\Eloquent\Model;


class Property extends Model
{
    //
//    use LogsActivity;

    const PROPERTY_TYPE_COMPOUND = 3;

    protected $guarded = ['id'];
    protected  $casts = [
        'document_required_for_contract'    => 'array',
        'prefered_nationality'    => 'array',
    ];


    public function images()
    {
        return $this->hasMany('App\Website\PropertyImage','property_id');
    }

    public function district()
    {
        return $this->hasOne('App\Website\District','id','location');
    }

    public function units()
    {
        return $this->hasMany('App\Website\Unit', 'property_id');
    }
}
