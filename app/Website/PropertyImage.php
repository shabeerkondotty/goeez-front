<?php

namespace App\Website;

use Illuminate\Database\Eloquent\Model;

class PropertyImage extends Model
{
    //
    protected $guarded = ['id'];

    public function property()
    {
        return $this->belongsTO('App\Website\Property', 'property_id');
    }
}
