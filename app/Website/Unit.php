<?php

namespace App\Website;

use App\Http\Utilities\CommonUtility;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    //

    const COMPOUND_UNIT_APARTMENT = 1;
    const COMPOUND_UNIT_ROOF_VILLA = 2;
    const COMPOUND_UNIT_STUDIO = 3;
    const COMPOUND_UNIT_SUITE = 4;
    const COMPOUND_UNIT_SINGLE_FLOOR = 5;
    const COMPOUND_UNIT_DUAL_FLOOR = 6;
    const COMPOUND_UNIT_TRIPLE_FLOOR = 7;

    const DIR_IMAGE = CommonUtility::GOEEZ_APP_LINK . 'public/unit/images/';

    protected $guarded = ['id'];


    public function property()
    {
        return $this->belongsTo('App\Website\Property','property_id');
    }

    public function images()
    {
        return $this->hasMany('App\Website\UnitImage','unit_id');
    }

    public function first_image()
    {
        $first_image = $this->images[0]['id'] . '_' . $this->images[0]['name'];
        return $first_image;
    }


}