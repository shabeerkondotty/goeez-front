<?php

namespace App\Website;

use Illuminate\Database\Eloquent\Model;

class UnitImage extends Model
{
    //
    protected $guarded = ['id'];

    public function unit()
    {
        return $this->belongsTO('App\Website\Unit', 'unit_id');
    }
}
