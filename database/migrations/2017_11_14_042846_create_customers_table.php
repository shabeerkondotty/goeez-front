<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->string('password');
            $table->integer('nationality');
            $table->smallInteger('status')->comment('0-notconfirmed,1-confirmed, 2-3rd step, 3-2nd&3rd')->default(0);
            $table->boolean('is_active')->comment('1-active,0-inactive')->default(0);
/*            $table->char('confirm_mail', 36);
            $table->char('confirm_phone', 36);*/
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
