<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->boolean('is_new')->default(0);
            $table->smallInteger('staying')->default(0);
            $table->smallInteger('family_type')->nullable();
            $table->smallInteger('living_type')->nullable();
            $table->integer('total_members')->default(0);
            $table->integer('total_bed')->default(0);
            $table->integer('total_room')->default(0);
            $table->integer('budget_f')->nullable();
            $table->integer('budget_t')->nullable();
            $table->boolean('is_agree')->default(0);
            $table->text('profile_pic')->nullable();
            $table->timestamps();
        });
        Schema::table('customer_details', function (Blueprint $table) {
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_details');
    }
}
