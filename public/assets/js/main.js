$(function(){
    new WOW().init();

    $('.mnt_slider').bxSlider({
        mode: 'fade',
        auto: true,
        speed: 3500,
        easing: 'linear',
        infiniteLoop: true
    });
});