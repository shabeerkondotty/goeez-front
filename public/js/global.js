$(document).ready(function() {

    /*X-CSRF-TOKEN*/
    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

    /*X-CSRF-TOKEN*/

    /*----------------- ajaxForm -------------------*/
    $(document).on("submit", 'form[data-plugin="ajaxForm"]', function (e) {
        e.preventDefault();

        var $this = $(this);
        var url = $this.attr("action");
        var method = $this.attr("method");
        var data = {};
        var processData = true;
        var contentType = "application/x-www-form-urlencoded";
        if ("POST" == method.toUpperCase() && $this.attr('enctype') == "multipart/form-data") {
            data = new FormData($this[0]);
            processData = false;
            contentType = false;
            //contentType = "multipart/form-data";
        } else {
            data = $this.serialize();
        }
        $.ajax({
            type: method,
            url: url,
            data:  data,
            dataType: 'json',
            processData : processData,
            contentType : contentType,
            success : function(data, textStatus, jqXHR) {
                $this.trigger('af.success', data, textStatus, jqXHR);
            },
            error : function(jqXHR, textStatus, errorThrown) {
                //alert("Error : " + errorThrown);
                var validator = $this.data("validator");
                if (validator && jqXHR.status == 422) {
                    var resposeJSON = $.parseJSON(jqXHR.responseText);
                    var errors = {};
                    $.each(resposeJSON, function (k, v) {
                        if(k == 'error'){

                        }

                        errors[k] = v;
                    });

                    if(!$.isEmptyObject(errors)) validator.showErrors(errors);
                } else {

                }

                $this.trigger('af.error', jqXHR.responseText, textStatus, jqXHR, errorThrown);
            },
            complete : function(jqXHR, textStatus) {
                var validator = $this.data("validator");
                $this.trigger('af.complete', jqXHR, textStatus);
            }
        });
    });
    /*----------------- ajaxForm END-------------------*/

});


