@extends('layouts.app')
@section('title','Register with Goeez')
@section('content')
<div class="content slideRight">
    <section class="tab-content-wrapper">
        <div class="tab-content">
            <!--banner tabs-->
            <div id="menu0" class="tab-pane fade in active">
                <div class="home-banner">
                    <img src="{{ asset('assets/images/bg.jpg') }}" alt="Goeez" class="img-responsive">
                    <div class="overlay-text">
                        <div class="heading"> we design thoughtful, liveable space</div>
                    </div>
                </div>
            </div>
            <div id="menu1" class="tab-pane fade">
                <div class="home-banner">
                    <img src="{{ asset('assets/images/bg.jpg') }}" alt="Goeez" class="img-responsive">
                    <div class="overlay-text">
                        <div class="heading"> relocation services made easier</div>
                    </div>
                </div>
            </div>
            <div id="menu2" class="tab-pane fade ">
                <div class="home-banner">
                    <img src="{{ asset('assets/images/bg.jpg') }}" alt="Goeez" class="img-responsive">
                    <div class="overlay-text">
                        <div class="heading"> manage your property with ease</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="padding: 50px 0;">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h5>Content will be updated soon</h5>
                    </div>
                </div>
            </div>
    </section>
</div>
@endsection
