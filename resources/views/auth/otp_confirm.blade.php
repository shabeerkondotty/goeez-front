@extends('layouts.app')
@section('title','Login')
@section('content')
    <div class="content slideRight">
        <section class="tab-content-wrapper">
            <div class="tab-content">
                <!--banner tabs-->
                <div id="menu0" class="tab-pane fade in active">
                    <div class="home-banner">
                        <img src="{{ asset('assets/images/bg.jpg') }}" alt="Goeez" class="img-responsive">
                        <div class="overlay-text">
                            <div class="heading"> we design thoughtful, liveable space</div>
                        </div>
                    </div>
                </div>
                <div id="menu1" class="tab-pane fade">
                    <div class="home-banner">
                        <img src="{{ asset('assets/images/bg.jpg') }}" alt="Goeez" class="img-responsive">
                        <div class="overlay-text">
                            <div class="heading"> relocation services made easier</div>
                        </div>
                    </div>
                </div>
                <div id="menu2" class="tab-pane fade ">
                    <div class="home-banner">
                        <img src="{{ asset('assets/images/bg.jpg') }}" alt="Goeez" class="img-responsive">
                        <div class="overlay-text">
                            <div class="heading"> manage your property with ease</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container" style="padding: 50px 0;">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <form class="form-horizontal" method="POST" action="{{ route('customer.validate.otp') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('goeez_otp') ? ' has-error' : '' }}">
                                <label for="goeez_otp" class="col-md-4 control-label">Enter OTP</label>

                                <div class="col-md-6">
                                    <input type="text" name="goeez_otp" id="goeez_otp" placeholder="Enter 6 digit OTP" />
                                    @if ($errors->has('goeez_otp'))
                                        <span class="help-block">
                                    {{ $errors->first('goeez_otp') }}
                          </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
