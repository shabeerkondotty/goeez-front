@extends('layouts.app')
@section('title','Login')
@section('content')
    <div class="content slideRight">
        <section class="tab-content-wrapper">
            <div class="tab-content">
                <!--banner tabs-->
                <div id="menu0" class="tab-pane fade in active">
                    <div class="home-banner">
                        <img src="{{ asset('assets/images/bg.jpg') }}" alt="Goeez" class="img-responsive">
                        <div class="overlay-text">
                            <div class="heading"> we design thoughtful, liveable space</div>
                        </div>
                    </div>
                </div>
                <div id="menu1" class="tab-pane fade">
                    <div class="home-banner">
                        <img src="{{ asset('assets/images/bg.jpg') }}" alt="Goeez" class="img-responsive">
                        <div class="overlay-text">
                            <div class="heading"> relocation services made easier</div>
                        </div>
                    </div>
                </div>
                <div id="menu2" class="tab-pane fade ">
                    <div class="home-banner">
                        <img src="{{ asset('assets/images/bg.jpg') }}" alt="Goeez" class="img-responsive">
                        <div class="overlay-text">
                            <div class="heading"> manage your property with ease</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container" style="padding: 50px 0;">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Send Password Reset Link
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
