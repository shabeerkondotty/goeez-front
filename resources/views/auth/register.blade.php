@extends('layouts.app')
@section('title','Register with Goeez')
@section('content')
        <!--registration-->
<section class="registration-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="form-wrapper">
                    <div class="panel-group" id="accordion">
                        <!--user details-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a ><span class="number-wrapper">1</span>User
                                        Details</a>
                                </h4>
                            </div>
                            <!-- toggle class '.in 'for show and hide-->
                            <div  class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <form action="{{ route('customer.register') }}" method="post" id="register_form_1" role="form" >
                                                <div class="form-item fields common-form">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                                                <label class="field-name">Full Name:</label>
                                                                <input type="text" class="form-control" name="name" value="{{ old('name') }}"
                                                                       id="name" placeholder="">
                                                                @if ($errors->has('name'))
                                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                                                <label class="field-name">Email address:</label>
                                                                <input type="email" class="form-control" name="email"
                                                                       id="email" placeholder="" value="{{ old('email') }}">
                                                                @if ($errors->has('email'))
                                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                                                <label class="field-name">Password:</label>
                                                                <input type="password" class="form-control" name="password"
                                                                       id="password" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                                                <label class="field-name">Confirm Password:</label>
                                                                <input type="password" class="form-control"
                                                                       name="password_confirmation" id="password_confirmation"
                                                                       placeholder="">
                                                                @if ($errors->has('password'))
                                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">

                                                            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                                                <label class="field-name">Mobile Number:</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" id="phone" name="phone" placeholder="" >
                                                                <span class="input-group-btn">
                                                                <button id="sign-in-button" type="button" class="btn-blue">Verify</button>
                                                                </span>
                                                                </div>
                                                                @if ($errors->has('phone'))
                                                                    <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="field-name">Enter the otp:</label>
                                                                <input type="text" class="form-control" name="goeez_otp"
                                                                       id="goeez_otp" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="field-name">Nationality:</label>
                                                                <select class="form-control select2" name="nationality">
                                                                    <option value="1">India</option>
                                                                    <option value="2"> Saudi Arabia</option>
                                                                    <option value="3">Oman</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="submit-wrapper text-center">
                                                                <button name="submit" type="submit" class="btn-blue">
                                                                    Submit
                                                                </button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--more details-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a ><span class="number-wrapper">2</span>More
                                        Details</a>
                                </h4>
                            </div>
                            <div  class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form action="{{ route('customer.register.second') }}" method="post" id="register_form_2" role="form" >
                                                <div class="form-item fields common-form">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <div class="radio-wrapper form-group">
                                                                    <h4>Are you new to Jeddah:</h4>
                                                                    <label class="radio-inline"><input type="radio" name="is_new" id="is_new_y" value="1"> Yes</label>
                                                                    <label class="radio-inline"><input type="radio" name="is_new" id="is_new_n" value="0" checked> No</label>
                                                                </div>
                                                                <div id="years-wrapper" class="years-wrapper row">
                                                                    <label class="field-name col-xs-4 col-sm-3">I'm here for :</label>
                                                                    <div class="col-xs-6 col-sm-5">
                                                                        <input type="text" class="form-control" name="staying" id="staying"  placeholder="Years">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="field-name">I'm looking for:</label>
                                                                <select class="form-control select2" name="family_type" id="family_type">
                                                                    <option value="" disabled selected>I'm looking for</option>
                                                                    <option value="1">Family Living</option>
                                                                    <option value="2"> Bachelor Living</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="field-name">Preferred Type of Living:</label>
                                                                <select class="form-control select2" name="living_type" id="living_type">
                                                                    <option value="" disabled selected>Preferred Type of Living
                                                                    </option>
                                                                    <option value="1">Apartment Living</option>
                                                                    <option value="2"> Villa Living</option>
                                                                    <option value="3">Compound Apartment
                                                                        Living
                                                                    </option>
                                                                    <option value="4">Compound Villa
                                                                        Living
                                                                    </option>
                                                                    <option value="5"> Any Compound Living
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="field-name">No of (family/friends) members going to stay in this apartment:</label>
                                                                <input type="text" class="form-control" name="total_members" id="total_members"
                                                                       placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="field-name">Preferred number of Bed Rooms:</label>
                                                                <input type="text" class="form-control" name="total_bed" id="total_bed"
                                                                       placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="field-name">Total number of rooms (including bedrooms + dining + sitting, etc):</label>
                                                                <input type="text" class="form-control" name="total_room" id="total_room"
                                                                       placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="field-name">Budget:</label>
                                                                <select class="form-control select2" name="budget" id="budget">
                                                                    <option value="" disabled selected>Budget</option>
                                                                    <option value="30-35">30 to 35</option>
                                                                    <option value="35-40"> 35 - 40</option>
                                                                    <option value="40-45">40 - 45</option>
                                                                    <option value="45-50">45 - 50</option>
                                                                    <option value="50-60">50 - 60</option>
                                                                    <option value="60-70">60 - 70</option>
                                                                    <option value="70-80">70 - 80</option>
                                                                    <option value="80-90">80 - 90</option>
                                                                    <option value="90-100">90 - 100</option>
                                                                    <option value="100-120">100- 120</option>
                                                                    <option value="120-140">120 - 140</option>
                                                                    <option value="140-160">140 - 160</option>
                                                                    <option value="160-180">160 - 180</option>
                                                                    <option value="180-200">180 - 200</option>
                                                                    <option value="200">200+</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="submit-wrapper text-center">
                                                                <button name="submit" type="submit" class="btn-blue ">
                                                                    Submit
                                                                </button>

                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Terms and conditions-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a ><span class="number-wrapper">3</span>Terms and Conditions</a>
                                </h4>
                            </div>
                            <div  class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form action="{{ route('customer.register.third') }}" method="post" id="register_form_3" role="form" >
                                                <div class="form-item fields common-form">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="scroll-wrapper">
                                                                <div class="terms-description">
                                                                    <p>
                                                                        We are trying to help the Professionals like you to find a suitable Apartment / Compound / Villa based on
                                                                        your budget as well as requirements by saving your time and energy. Our service will help you to relax and
                                                                        get away from all the stress, hard time, unpleasant and unpredictable negative surprises. For this we charge a nominal "Service Fee".
                                                                        You need to pay us this "Service Fee" <b>ONLY AFTER</b> you decide to rent a property within our list.
                                                                    </p>
                                                                    <p>
                                                                        Once you agree with our "Terms and Conditions" mentioned below, please reply to this
                                                                        mail to proceed further. Upon the receipt of your application, we will send you the
                                                                        complete details of the properties (including it's video) via email. Once you are convinced about an apartment/villa
                                                                        through the pictures/videos, we can go for the site visit for the finalization.
                                                                    </p>
                                                                </div>
                                                                <div class="term_items">
                                                                    <h5>Terms and Conditions :</h5>
                                                                    <ul>
                                                                        <li>
                                                                            The "Service Fee" is One Month rent of the apartment/villa you are going to rent.
                                                                            ( <b><i>For example</i></b> : If it is a 60,000/year apartment, then the service charge is 5000 SAR, which is one month rent of 60K) (One time payment).
                                                                        </li>
                                                                        <li>
                                                                            Our service is limited to find suitable apartments, advertising/announcing the information to the public through different medias, sharing with you the details of the same,
                                                                            then showing you the best apartments and letting you to choose the best one that fits your needs.
                                                                        </li>
                                                                        <li>
                                                                            Once you like an apartment and the agreement is signed, all further things need to be discussed/resolved by yourself with the owner directly as usual.
                                                                            We will not be responsible for anything which comes after signing the contract.
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="checkbox-agree">
                                                                <label class="checkbox-label">
                                                                    <input type="checkbox" name="is_agree" value="1" /> Agree with the terms and conditions
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="submit-wrapper text-center">
                                                                <button name="submit" type="submit" class="btn-blue ">
                                                                    Submit
                                                                </button>

                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('page_scripts')



<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase.js"></script>
<script>
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyDnYdgUH4KPIcX-qMUkSvJpY5qH-GyZxAs",
        authDomain: "goeez-web.firebaseapp.com",
        databaseURL: "https://goeez-web.firebaseio.com",
        projectId: "goeez-web",
        storageBucket: "goeez-web.appspot.com",
        messagingSenderId: "899439375446"
    };
    firebase.initializeApp(config);


    firebase.auth().languageCode = 'en';




    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
        'size': 'invisible',
        'callback': function(response) {
            // reCAPTCHA solved, allow signInWithPhoneNumber.
        }
    });
</script>
<script>
    $(document).ready(function() {

        $("input[name='is_new']").click(function() {

            console.log($(this).val());
            if($(this).val()==1) {
                $('#years-wrapper').addClass('hidden');
            }else {
                $('#years-wrapper').removeClass('hidden');
            }
        });

        jQuery.validator.addMethod("phoneno", function(phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            return this.optional(element) || phone_number.length == 10 &&
                    phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
        }, "Please specify a valid phone number");
        var $validator = $('#register_form_1').validate({
            rules: {
                name: {
                    required: true
                },
                email: {
                    email:true,
                    required: true
                },
                password: {
                    required : true,
                    minlength : 6
                },
                password_confirmation: {
                    equalTo: "#password"
                },
                phone: {
                    required: true,
                    /*phoneno: true,*/
                },
                goeez_otp: {
                    required: true,
                    digits: true,
                    minlength : 6
                }
            },
            messages: {
                name: {
                    required: "Full Name is required"
                },
                email: {
                    required: "Email is required",
                    email: "Enter valid email"
                },
                password: {
                    required: "Enter Password",
                    minlength: "Password must contain atleast 6 characters"
                }
                ,password_confirmation: {
                    equalTo: "Password confirmation failed"
                },
                phone: {
                    required: "Phone number is required",
                    phoneno: "Invalid Phone number",
                },
                goeez_otp: {
                    required: "Enter 6 digit OTP",
                    digits: "Invalid OTP",
                    minlength: "Invalid OTP",
                }
            }
        });

        $('#sign-in-button').click(function() {
            var phoneNumber = '+91'+$('#phone').val();
            onSignInSubmit(phoneNumber);
            /*console.log(phoneNumber);*/
        });

        $(document).on("submit", '#register_form_1', function (e) {
            e.preventDefault();
            var $this = $(this);
            var verify_code = verifyCode($this);
        });



        var $validator2 = $('#register_form_2').validate({
            rules: {
                family_type: {
                    required : true
                },
                living_type: {
                    required: true
                },
                total_members: {
                    required: true,
                    digits: true
                },
                total_bed: {
                    required: true,
                    digits: true
                },
                total_room: {
                    required: true,
                    digits: true
                },
                budget: {
                    required: true
                }

            },
            messages: {
            }
        });


        $(document).on("submit", '#register_form_2', function (e) {
            e.preventDefault();
            var $this = $(this);
            ajaxSubmit($this);
        });


        var $validator3 = $('#register_form_3').validate({
            rules: {
                is_agree: {
                    required: true
                }

            },
            messages: {

            }
        });

        $(document).on("submit", '#register_form_3', function (e) {
            e.preventDefault();
            var $this = $(this);
            ajaxSubmit($this);
        });


        $(document).on('af.success','#register_form_1',function(e,data) {
            $(this).closest('.panel-collapse').removeClass('in');
            $('#register_form_2').closest('.panel-collapse').addClass('in');
            $('#cust-signin').remove();
            $('#cust-register').remove();
            $('#logout-display').removeClass('hidden');
        });

        $(document).on('af.success','#register_form_2',function(e,data) {
            $(this).closest('.panel-collapse').removeClass('in');
            $('#register_form_3').closest('.panel-collapse').addClass('in');
        });

        $(document).on('af.success','#register_form_3',function(e,data) {
            console.log('2nd');
            $(this).closest('.panel-collapse').removeClass('in');
            window.location.replace("{{ route('index') }}");
        });

    });
</script>
<script>
    function onSignInSubmit(phoneNumber) {
        var appVerifier = window.recaptchaVerifier;
        firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
                .then(function (confirmationResult) {
                    // SMS sent. Prompt user to type the code from the message, then sign the
                    // user in with confirmationResult.confirm(code).
                    window.confirmationResult = confirmationResult;
                    /*console.log(confirmationResult);*/
                    $('#sign-in-button').attr('disabled','');
                    $('#phone').attr('readonly','');
                    $('#phone').val(phoneNumber.replace('+', ''));
                }).catch(function (error) {
            // Error; SMS not sent
            // ...
        });
    }

    function verifyCode($this){
        var code =  document.getElementById('goeez_otp').value
        confirmationResult.confirm(code).then(function (result) {
            // User signed in successfully.
            var user_id = result.user;
            ajaxSubmit($this);

        }).catch(function (error) {
            // User couldn't sign in (bad verification code?)
            // ...
            var msg_content =  '<label id="otp-error" class="error" for="goeez_otp">' +
                    error.message + '</label>';
            $('#goeez_otp').closest('.form-group').addClass('has-error');
            $('#goeez_otp').closest('.form-group').append(msg_content);
        });


    }

    function ajaxSubmit($this) {
        var url = $this.attr("action");
        var method = $this.attr("method");
        var data = {};
        var processData = true;
        var contentType = "application/x-www-form-urlencoded";
        if ("POST" == method.toUpperCase() && $this.attr('enctype') == "multipart/form-data") {
            data = new FormData($this[0]);
            processData = false;
            contentType = false;
            //contentType = "multipart/form-data";
        } else {
            data = $this.serialize();
        }
        $.ajax({
            type: method,
            url: url,
            data:  data,
            dataType: 'json',
            processData : processData,
            contentType : contentType,
            success : function(data, textStatus, jqXHR) {
                $this.trigger('af.success', data, textStatus, jqXHR);
            },
            error : function(jqXHR, textStatus, errorThrown) {
                //alert("Error : " + errorThrown);
                var validator = $this.data("validator");
                if (validator && jqXHR.status == 422) {
                    var resposeJSON = $.parseJSON(jqXHR.responseText);
                    var errors = {};
                    $.each(resposeJSON, function (k, v) {
                        if(k == 'error'){

                        }

                        errors[k] = v;
                    });

                    if(!$.isEmptyObject(errors)) validator.showErrors(errors);
                } else {

                }

                $this.trigger('af.error', jqXHR.responseText, textStatus, jqXHR, errorThrown);
            },
            complete : function(jqXHR, textStatus) {
                var validator = $this.data("validator");
                $this.trigger('af.complete', jqXHR, textStatus);
            }
        });
    }
</script>
@endpush