<section class="footer-wrapper" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="logo-description">
                    <img src="{{ asset('assets/images/W-LOGO.png') }}" alt="Goeez" class="logo">
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <div class="logo-description">
                    <div class="des">
                        <p>
                            The company principle of Architecture-Studio
                            is the collective conception. From the very beginning, the practice has believed in the virtues
                            of exchange. crossing ideas, common effort, shared knowledge and enthusiasm.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-offset-4 col-lg-4">
                <div class="quick-contact">
                    <ul>
                        <li class="number"><a href="">+999999999999</a></li>
                        <li class="mail"><a href="mailto: customercare@riyalgold.com" target="_blank"> info@Goeez.com</a></li>
                        <li class="address">
                            Litaka, Jungmannova 35/29, <br>
                            Nove Me'sto,Czech Republic
                        </li>

                    </ul>
                    <div class="social-wrapper">
                        <ul class="list-inline">
                            <li><a href="" target="_blank">
                                    <img src="{{ asset('assets/images/FA.png') }}" alt="facebook">
                                </a></li>
                            <li><a href="" target="_blank">
                                    <img src="{{ asset('assets/images/INS.png') }}" alt="insta">
                                </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>