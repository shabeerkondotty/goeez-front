<header class="top-header cbp-af-header">
    <nav class="navbar">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="./" class="logo"><img src="{{ asset('assets/images/logo.png') }}" alt="Goeez" class="img-logo"></a>
        </div>

        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="./" class="active"> Individual</a></li>
                <li><a href="./"> Corporate</a></li>
                <li><a href="./"> About</a></li>
                <li><a href="./"> Contact</a></li>
                @if (Auth::guard('web_customer')->guest())
                    <li id="cust-signin" class="singn-in"><a href="{{ route('customer.login.view') }}"> Sign In</a></li>
                    <li id="cust-register" class="singn-in"><a href="{{ route('customer.signup') }}"> Register</a></li>
                @else
                    <li >
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            My Account <span class="caret"></span> </a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                            <li><a tabindex="-1" href="#">Welcome {{ Auth::guard('web_customer')->user()->name }}</a></li>
                            <li><a tabindex="-1" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{ route('customer.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif

                <li id="logout-display" class="hidden" >
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        My Account <span class="caret"></span> </a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                        <li><a tabindex="-1" href="#">Welcome</a></li>
                        <li><a tabindex="-1" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                            <form id="logout-form" action="{{ route('customer.logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>

            </ul>

        </div><!--/.nav-collapse -->
    </nav>
</header>
@push('page_style')
<style>
    .nav .open>a, .nav .open>a:focus, .nav .open>a:hover
</style>
@endpush