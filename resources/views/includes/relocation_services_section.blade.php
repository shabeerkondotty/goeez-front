<div id="menu11" class="tab-pane fade">
    <section class="relocation-wrapper">
        <div class="row">
            <div class="col-sm-4">
                <div class="image-tile">
                    <a href="./relocating.php" class="img">
                        <img src="{{ asset('assets/images/i-r.png') }}" alt="goeez" class="img-responsive">
                    </a>
                    <div class="title_thumbs text-center">
                        I'm Relocating
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="image-tile text-center">
                    <a href="./relocating.php" class="img">
                        <img src="{{ asset('assets/images/c-r.png') }}" alt="goeez" class="img-responsive">
                    </a>
                    <div class="title_thumbs">
                        Corporate Relocation
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="image-tile text-center">
                    <a href="./relocating.php" class="img">
                        <img src="{{ asset('assets/images/a-f-r-c.png') }}" alt="goeez" class="img-responsive">
                    </a>
                    <div class="title_thumbs">
                        Assistance for Relocation Companies
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="description_wrapper">
                    <p>
                        Whether you are a Corporate Company, Individual or an International Relocation Services Company,
                        we have many things to offer you. Being the Pioneer in Relocation Consultancy Services provider in this local market,
                        and the one and professional services company, we have much reliable and professional services to offer you.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="image-tile">
                    <a href="./relocating.php" class="img">
                        <img src="{{ asset('assets/images/l-f-h.png') }}" alt="goeez" class="img-responsive">
                    </a>
                    <div class="title_thumbs">
                        I'm looking for a home
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="image-tile text-center">
                    <a href="./relocating.php" class="img">
                        <img src="{{ asset('assets/images/l-f-c.png') }}" alt="goeez" class="img-responsive">
                    </a>
                    <div class="title_thumbs">
                        I'm looking for a Compound
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="image-tile text-center">
                    <a href="./relocating.php" class="img">
                        <img src="{{ asset('assets/images/i-n-p-a.png') }}" alt="goeez" class="img-responsive">
                    </a>
                    <div class="title_thumbs">
                        I need personalized assistance
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="description_wrapper">
                    <p>
                        Whether you are a Corporate Company, Individual or an International Relocation Services Company,
                        we have many things to offer you. Being the Pioneer in Relocation Consultancy Services provider in this local market,
                        and the one and professional services company, we have much reliable and professional services to offer you.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="image-tile text-center">
                    <a href="./relocating.php" class="img">
                        <img src="{{ asset('assets/images/s-i-s.png') }}" alt="goeez" class="img-responsive">
                    </a>
                    <div class="title_thumbs">
                        Settle-in Services
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="image-tile text-center">
                    <a href="./relocating.php" class="img">
                        <img src="{{ asset('assets/images/s-s-s.png') }}" alt="goeez" class="img-responsive">
                    </a>
                    <div class="title_thumbs">
                        School Search Support
                    </div>
                </div>

            </div>
            <div class="col-sm-4">
                <div class="image-tile text-center">
                    <a href="./relocating.php" class="img">
                        <img src="{{ asset('assets/images/o-t-.png') }}" alt="goeez" class="img-responsive">
                    </a>
                    <div class="title_thumbs">
                        Orientation Tours
                    </div>
                </div>
            </div>
        </div>
        <!--enquiry form-->
        <div class="row">
            <div class="col-sm-12">
                <div class="enquiry_wrapper">
                    <div class="title_contact">
                        <h1>Make an Enquiry</h1>
                    </div>
                    <div class="contact-form">
                        <div class="form-wrapper">
                            <form class="form"  id="contact-form" method="post" action="" role="form">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control custom-control" id="form_name" name="name" placeholder=" Name">
                                            <div class="help-block with-errors"></div>
                                        </div>

                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="email" class="form-control custom-control" id="mobile" name="mobile" placeholder="Phone">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="email" class="form-control custom-control" id="mobile" name="mobile" placeholder="Mail">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <select class="form-control" id="sel1">
                                                <option selected="selected">Choose your service</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <textarea class="custom-control" rows="2" name="message" id="form_message" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="btn-wrapper">
                                            <button type="submit" class="submit-btn" id="submit">Send</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>