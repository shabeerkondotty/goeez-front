<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow">
    <meta name="description" content="Goeez Relocation Consultancy">
    <meta name="keyword" content="Goeez Relocation Consultancy">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>{{ config('app.name') }} | @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="{{ asset('theme/images/favicon.ico') }}" type="image/x-icon">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/global.css') }}" rel="stylesheet">
    @stack('page_style')

</head>
<body>
<div class="pre-loader"></div>
@include('includes.header')
@yield('content')
@include('includes.footer')

<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/imagesloaded.pkgd.js') }}"></script>
<script src="{{ asset('assets/js/cbpAnimatedHeader.min.js') }}"></script>
<script src="{{ asset('assets/js/classie.js') }}"></script>
<script src="{{ asset('assets/js/scrollreveal.min.js') }}"></script>
<script src="{{ asset('assets/js/wow.min.js') }}"></script>
<script src="{{ asset('assets/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/js/slider.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script src="{{ asset('js/global.js') }}"></script>
<!-- VALIDATOR -->
<script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-validation/additional-methods.min.js') }}"></script>
<!--// VALIDATOR -->

<script>
    var config = {
        viewFactor : 0.15,
        duration   : 1500,
        distance   : "0px",
        scale      : 0.9
    }
    var block = {
        reset: true,
        viewOffset: { top: 64 }
    }

    window.sr = new ScrollReveal(config)
    sr.reveal('.fooReveal', block);

    $(".select2").select2({
        theme: "bootstrap"

    });
</script>

@stack('page_scripts')
</body>
</html>