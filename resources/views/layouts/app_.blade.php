<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow">
    <meta name="description" content="Surgical Equipments">
    <meta name="keyword" content="Surgical Equipments">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>{{ config('app.name') }} | @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="{{ asset('theme/images/favicon.ico') }}" type="image/x-icon">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet">
    @stack('page_style')
    <script src="https://www.gstatic.com/firebasejs/4.8.1/firebase.js"></script>
    <script>
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyDnYdgUH4KPIcX-qMUkSvJpY5qH-GyZxAs",
            authDomain: "goeez-web.firebaseapp.com",
            databaseURL: "https://goeez-web.firebaseio.com",
            projectId: "goeez-web",
            storageBucket: "goeez-web.appspot.com",
            messagingSenderId: "899439375446"
        };
        firebase.initializeApp(config);


        firebase.auth().languageCode = 'en';


        var phoneNumber = "+918111951972";

        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
            'size': 'visible',
            'callback': function(response) {
                // reCAPTCHA solved, allow signInWithPhoneNumber.
                onSignInSubmit();
            }
        });


        function onSignInSubmit() {
            alert("onSignIn");
            var appVerifier = window.recaptchaVerifier;
            firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
                    .then(function (confirmationResult) {
                        // SMS sent. Prompt user to type the code from the message, then sign the
                        // user in with confirmationResult.confirm(code).
                        window.confirmationResult = confirmationResult;
                    }).catch(function (error) {
                // Error; SMS not sent
                // ...
            });
        }

    </script>
</head>
<body>
<div class="pre-loader"></div>
@include('includes.header')
@yield('content')
@include('includes.footer')

<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/imagesloaded.pkgd.js') }}"></script>
<script src="{{ asset('assets/js/cbpAnimatedHeader.min.js') }}"></script>
<script src="{{ asset('assets/js/classie.js') }}"></script>
<script src="{{ asset('assets/js/scrollreveal.min.js') }}"></script>
<script src="{{ asset('assets/js/wow.min.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script>
    var config = {
        viewFactor : 0.15,
        duration   : 1500,
        distance   : "0px",
        scale      : 0.9
    }
    var block = {
        reset: true,
        viewOffset: { top: 64 }
    }

    window.sr = new ScrollReveal(config)
    sr.reveal('.fooReveal', block);
</script>
@stack('page_scripts')
</body>
</html>