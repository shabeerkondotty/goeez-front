@extends('layouts.app')
@section('title','Relocation Consultancy')
@section('content')
    <div class="content slideRight">
        <section class="tab-content-wrapper">
            <div class="tab-content">
                <!--banner tabs-->
                <div id="menu0" class="tab-pane fade in active">
                    <div class="home-banner">
                        <img src="{{ asset('assets/images/bg.jpg') }}" alt="Goeez" class="img-responsive">
                        <div class="overlay-text">
                            <div class="heading"> we design thoughtful, liveable space</div>
                        </div>
                    </div>
                </div>
                <div id="menu1" class="tab-pane fade">
                    <div class="home-banner">
                        <img src="{{ asset('assets/images/bg.jpg') }}" alt="Goeez" class="img-responsive">
                        <div class="overlay-text">
                            <div class="heading"> relocation services made easier</div>
                        </div>
                    </div>
                </div>
                <div id="menu2" class="tab-pane fade ">
                    <div class="home-banner">
                        <img src="{{ asset('assets/images/bg.jpg') }}" alt="Goeez" class="img-responsive">
                        <div class="overlay-text">
                            <div class="heading"> manage your property with ease</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tab-menu">
                            <!--tabs-->
                            <ul class="nav nav-pills">
                                <li class="active"><a data-toggle="tab" href="#menu0" data-target="#menu0, #menu01">Compounds in jeddah</a></li>
                                <li><a data-toggle="tab" href="#menu1" data-target="#menu1, #menu11">Relocation Services</a></li>
                                <li><a data-toggle="tab" href="#menu2" data-target="#menu2, #menu21">Management Services</a></li>
                            </ul>
                        </div>

                        <div class="tab-content">

                            <!--compound in jeddah starts-->
                            @include('includes.compounds_section')
                            <!--compound in jeddah ends-->


                            <!--relocation services starts -->
                            @include('includes.relocation_services_section')
                            <!--relocation services ends -->

                            <!--management services starts-->
                            @include('includes.management_services_section')
                            <!--management services ends-->

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection