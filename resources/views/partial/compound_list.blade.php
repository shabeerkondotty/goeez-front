@foreach($compounds as $compound)
    <div class="col-sm-6 fooReveal mb30">
        <div class="property-item">
            <div class="row">
                <div class="col-sm-6 p-r-0">
                    <figure>
                        <a href="#">
                            <img src="{{ \App\Website\Unit::DIR_IMAGE . $compound->first_image() }}" class="img-responsive" alt="Goeez">
                            <div class="overlay-prop">
                                <span><i class="fas fa-camera"></i>&nbsp; {{ $compound->images->count() }} photos</span>
                                <span><i class="fas fa-map-marker-alt"></i> &nbsp; {{$compound->property->district->name}}</span>
                            </div>
                        </a>
                    </figure>
                </div>
                <div class="col-sm-6">
                    <div class="detail">
                        <h3 class="price">SAR {{$compound->annual_rent}}</h3>
                        <h5>{{$compound->online_description}}</h5>
                        <div class="property-meta">
                            <span><i class="fas fa-bed"></i> 6</span>
                            <span><i class="fas fa-bath"></i> 5</span>
                            <span><i class="fas fa-car"></i> 4</span>
                            <span>Land Sze : {{$compound->total_area}} m<sup>2</sup></span>
                        </div>
                        <p>{{$compound->main_description}}
                        </p>
                        {{--<a class="more-details" href="{{ Auth::guard('web_customer')->guest() ? route('customer.login.view') : Auth::guard('web_customer')->user()->status!=1 ? route('customer.register.complete') : route('details') }}">More Details <i class="fas fa-caret-right"></i></a>--}}
                        <a class="more-details" href="{{ Auth::guard('web_customer')->guest() ? route('customer.login.view') : (Auth::guard('web_customer')->user()->status!=1 ? route('customer.register.complete') : route('details')) }}">More Details <i class="fas fa-caret-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <ul class="listings">
            <li class="arrow wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.1s" >
                <a href="">
                    <img src="{{ asset('assets/images/arrow-down.jpg') }}">
                    <i class="fab fa-facebook-f"></i>
                </a>
            </li>
            <li class="arrow wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.2s">
                <a href="">
                    <img src="{{ asset('assets/images/arrow-down.jpg') }}">
                    <i class="fab fa-twitter"></i>
                </a>
            </li>
            <li class="arrow wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <a href="">
                    <img src="{{ asset('assets/images/arrow-down.jpg') }}">
                    <i class="fab fa-whatsapp-square"></i>
                </a>
            </li>
            <li class="arrow wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.4s">
                <a href="">
                    <img src="{{ asset('assets/images/arrow-down.jpg') }}">
                    <i class="fab fa-linkedin-in"></i>
                </a>
            </li>
            <li class="arrow wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                <a href="">
                    <img src="{{ asset('assets/images/arrow-down.jpg') }}">
                    <i class="fas fa-at"></i>
                </a>
            </li>
        </ul>
    </div>
@endforeach