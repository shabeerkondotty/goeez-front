<div class="modal fade" id="myLoginModal">
    <div class="modal-dialog ">
        <form class="login modal-content" action="{{ route('customer.login') }}" method="post" action='' id="login_form" role="form" data-plugin="ajaxForm">
            <!--<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h3>Login to MyWebsite.com</h3>
            </div>-->
            <div class="modal-body row" >
                <div class="close_btn">
                    <button type="button" class="close" data-dismiss="modal">✕</button>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12" style="padding: 12px 6%;">
                    <h2>Login</h2>
                    <p>Get Access to your account</p>
                </div>
                <div class="right-modal-content col-md-6 col-sm-12 col-xs-12" style="padding: 30px 2%;">
                    <div class="form-group @if ($errors->has('email')) has-error @endif">
                        <input type="text" name="email" id="email" placeholder="Email" />
                        @if ($errors->has('email'))
                        <span class="help-block">
                          {{ $errors->first('email') }}
                        </span>
                        @endif
                    </div>
                    <div class="form-group @if ($errors->has('password')) has-error @endif">
                        <input type="password" id="password" name="password" placeholder="Password" />
                        @if ($errors->has('password'))
                        <span class="help-block">
                            {{ $errors->first('password') }}
                        </span>
                        @endif
                    <input type="submit" value="Login" class="btn btn-lg" />
                    <a data-toggle="modal" href="#mySignupModal" class="btn btn-lg signup_button" >New to Kerala HealthMart? Sign up</a>
                    <div class="remember-forgot">
                        <div class="row">
                            <!--<div class="col-md-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" />
                                        Remember Me
                                    </label>
                                </div>
                            </div>-->
                            <div class="col-md-12 forgot-pass-content">
                                <a href="javascript:void(0)" class="forgot-pass pull-right">Forgot Password</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <!--<div class="modal-footer">
                New To MyWebsite.com?
                <a href="#" class="btn btn-primary">Register</a>
            </div>-->
        </form>
    </div>
</div>
@push('page_scripts')
<script>
    $(document).ready(function() {
        $('#myLoginModal').on('af.complete','#login_form',function(e,data) {
            $('#myLoginModal').modal('hide');
            location.reload();
        });

        var $validator = $('#login_form').validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password : {
                    required : true,
                    minlength: 6
                }
            },
            messages: {
                email: {
                    required: "Email is required"
                },
                password: {
                    required: "Password is required",
                    minlength: "Password must have 6 charecters"
                }
            }
        });


    });
</script>
@endpush