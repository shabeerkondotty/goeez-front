<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/test', ['as' => 'test', 'uses' => 'CustomerAuth\RegisterController@test']);
Route::group(['middleware' => ['setTheme:default']], function () {
    Route::group(['namespace' => 'CustomerAuth','as' => 'customer.'], function () {
        Route::group(['middleware' => 'customer_guest'], function () {
            Route::get('/signup', ['as' => 'signup', 'uses' => 'RegisterController@showRegistrationForm']);
            Route::post('/customer_register', ['as' => 'register', 'uses' => 'RegisterController@register']);
            Route::get('/validate/otp', ['as' => 'otp.view', 'uses' => 'RegisterController@showOtp']);
            Route::post('/validate/otp', ['as' => 'validate.otp', 'uses' => 'RegisterController@validate_otp']);
            Route::post('/resend_otp', ['as' => 'resend.otp', 'uses' => 'RegisterController@resend_otp']);
            Route::get('/login', ['as' => 'login.view', 'uses' => 'LoginController@showLoginForm']);
            Route::post('/login', ['as' => 'login', 'uses' => 'LoginController@login']);
        });
        Route::group(['middleware' => 'customer_auth'], function() {
            Route::post('/customer_logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);

        });
    });
    Route::group(['namespace' => 'front'], function () {
        Route::get('/', ['as' => 'index', 'uses'=> 'HomeController@index']);

        Route::group(['middleware' => 'customer_auth'], function () {
            Route::get('/details', ['as' => 'details', 'uses' => 'HomeController@details']);
            Route::get('/registration/complete', ['as' => 'customer.register.complete', 'uses'=> 'HomeController@signup_complete']);
            Route::post('/customer_register_s', ['as' => 'customer.register.second', 'uses' => 'HomeController@register_second']);
            Route::post('/customer_register_t', ['as' => 'customer.register.third', 'uses' => 'HomeController@register_third']);
        });
    });

});